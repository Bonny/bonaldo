(function () {
    'use strict';

    var app = angular.module('app', [
        'ngStorage',
        'angular-loading-bar',
        'ui.router',
        'ui.router.breadcrumbs',
        'ngAnimate',
        'gettext',
        'ngFileUpload'
    ]);

    var BASE = "./";

    var ver = "1.0.0";

    //FIXME dev
    ver = "." + new Date().getTime();

    app.constant('info', {
        name: "Bancaetica",
        version: ver,
        relDate: "14/06/2018"
    });

    app.constant('urls', {
        baseUrl: BASE,
        auth: BASE + '/v1/authentication',
        config: BASE + '/v1/config',
        logflush: BASE + '/v1/logflush'
    });

    app.run(['$rootScope', '$state', '$stateParams', 'gettextCatalog',
        function ($rootScope, $state, $stateParams, gettextCatalog) {

            gettextCatalog.currentLanguage = 'it';

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

        }]);

    app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'cfpLoadingBarProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider, cfpLoadingBarProvider) {

            $httpProvider.interceptors.push('$httpInterceptor');

            ////// ui route config //////

            $urlRouterProvider.otherwise('/index');

            function check_auth($state, $auth) {
                if (!$auth.isAuth()) {
                    $state.go('signin');
                }
            }

            $stateProvider
                    .state('logs', {
                        url: '/logs',
                        controller: ['$scope', '$logger', function ($scope, $logger) {
                                $scope.getContent = function () {
                                    return $logger.getAll();
                                };
                            }],
                        template: '<button ui-sref="index" type="button" class="btn btn-link mx-auto mb-3">' +
                                '<i class="fa fa-long-arrow-alt-left"></i> <span translate>back</span>' +
                                '</button>' +
                                '<pre ng-repeat="i in getContent() track by $index" class="m-0">{{i}}</pre>'
                    })
                    .state('signin', {
                        url: '/login',
                        controller: 'SigninCtrl',
                        templateUrl: 'partials/signin.html'
                    })
                    .state('home', {
                        url: '/home',
                        controller: 'HomeCtrl',
                        templateUrl: 'partials/home.html',
                        onEnter: check_auth,
                        breadcrumb: {
                            text: "Home",
                            stateName: 'home'
                        }
                    })
                    .state('upload', {
                        url: '/upload',
                        templateUrl: 'partials/upload.html'
                    })
                    .state('index', {
                        url: '/index',
                        cotroller: "MainCtrl",
                        template: '<div class="card text-center mx-auto mt-3" ng-init="init()" style="border:0;width: 40rem;">' +
                                '    <div class="card-body mt-3">' +
                                '      <i class="fa fa-spinner fa-pulse fa-2x"></i>' +
                                '      <p class="mt-3" translate>loading</p>' +
                                '    </div>' +
                                '  </div>'
                    });

        }
    ]);

})();