var _TIMEOUT_ = 500;

(function () {
    'use strict';

    angular.module('app').factory('$logger', ['$log', '$rootScope', function ($log, $rootScope) {
            var store = [], current = [];
            function flush() {
                var data = [];
                data.concat(current);
                current = [];
                $rootScope.$broadcast('$LogFlush', data);
            }
            function log(level, msg) {
                current.push(msg);
                store.push(msg);
                switch (level) {
                    case "INFO":
                        $log.info(msg);
                        break;
                    case "WARN":
                        $log.warn(msg);
                        break;
                    case "DEBUG":
                        $log.debug(msg);
                        break;
                    case "ERROR":
                        $log.error(msg);
                        break;
                }
            }
            return {
                info: function (msg) {
                    log("INFO", msg);
                },
                warn: function (msg) {
                    log("WARN", msg);
                },
                debug: function (msg) {
                    log("DEBUG", msg);
                },
                error: function (msg) {
                    log("ERROR", msg);
                },
                getAll: function () {
                    return angular.copy(store);
                }
            };
        }
    ]);

    angular.module('app').factory('$httpInterceptor', ['$q', '$localStorage', '$logger', '$rootScope', 'urls', '$utils', 'info',
        function ($q, $localStorage, $logger, $rootScope, urls, $utils, info) {

            var v = info.version.replace(".", "");

            function isRestful(config) {
                return (config && config.url && config.url.indexOf(urls.baseUrl) === 0);
            }

            function logRequest(config) {
                config.requestTimestamp = new Date().getTime();
                config.requestID = config.requestTimestamp;
                $logger.debug("[" + config.requestID + "] Request [" + config.method + "] " + config.url);
                if (isRestful(config))
                    config.url = config.url + "?_rID=" + config.requestTimestamp;
                else
                    config.url = config.url + "?v=" + v;
            }

            function logResponse(config, error) {
                var info = "[" + config.requestID + "] Response to [" + config.method + "] " + config.url + " request in " + (new Date().getTime() - config.requestTimestamp) + "ms";
                if (!error)
                    $logger.debug(info);
                else
                    $logger.error(info + " - " + JSON.stringify($utils.getError(error)));
            }

            return {
                request: function (config) {
                    logRequest(config);
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers['Authorization'] = 'Bearer ' + $localStorage.token;
                    }
                    config.headers['Access-Control-Allow-Origin'] = '*';
                    config.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS, HEAD';
                    config.headers['Access-Control-Allow-Headers'] = 'origin, content-type, accept, authorization';
                    return config;
                },
                response: function (response) {
                    var config = response.config;
                    logResponse(config);
                    return response;
                },
                responseError: function (response) {
                    var config = response.config;
                    logResponse(config, response.data || {});
                    if (response.status === 401) {
                        delete $localStorage.token;
                        $rootScope.$broadcast('$TokenExpired');
                    }
                    return $q.reject(response);
                }
            };
        }]);

    angular.module('app').factory('$utils', [function () {
            // default
            var ex = {code: "UNKNOW", reason: "unknow-error", status: 500};
            return {
                getError: function (response) {
                    return response ? angular.extend(ex, response) : ex;
                },
                isNullOrEmpty: function (value) {
                    return this.count(value) === 0;
                },
                count: function (value) {
                    return (value && angular.isDefined(value) ? value.length : 0);
                },
                isPlainText: function (value) {
                    return true;///^(\w*|\s*)[\w\s]*(\w*|\s*)$/g.test(value);
                },
                isNumber: function (value) {
                    return /^\d*[1-9]\d*$/g.test(value) || /^-\d*[1-9]\d*$/g.test(value) || /0/g.test(value);
                },
                isEmail: function (value) {
                    return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/g.test(value);
                },
                isBool: function (b) {
                    return (/^true$/i).test(b);
                }
            };
        }
    ]);

    angular.module('app').factory('$config', ['$http', 'urls', '$utils', '$timeout', function ($http, urls, $utils, $timeout) {
            var init = false;
            var config = {};
            return {
                init: function (success) {//TODO: gestire errore
                    $timeout(function () {
                        init = true;
                        success();
                    }, _TIMEOUT_);
                },
                isInit: function () {
                    return init;
                },
                getConfig: function () {
                    return angular.extend({}, config);
                }
            };
        }
    ]);

    angular.module('app').factory('$auth', ['$http', '$localStorage', 'urls', '$timeout',
        function ($http, $localStorage, urls, $timeout) {

            function getClaimsFromToken() {
                var token = $localStorage.token;
                var user = {};
                if (typeof token !== 'undefined') {
                    user = JSON.parse($localStorage.token);
                }
                return user;
            }

            var tokenClaims = getClaimsFromToken();
            var user = tokenClaims ? angular.fromJson(tokenClaims.user) : {};

            return {
                signin: function (data, success, error) {
                    $timeout(function () {
                        $localStorage.token = new Date().getTime();
                        user = {username: "Mr Demo"};
                        tokenClaims = {user: user};
                        success();
                    }, _TIMEOUT_);
                },
                logout: function (success) {
                    tokenClaims = {};
                    user = {};
                    delete $localStorage.token;
                    if (success)
                        success();
                },
                getTokenClaims: function () {
                    return tokenClaims;
                },
                isAuth: function () {
                    return $localStorage.token ? true : false;
                },
                getUser: function () {
                    return user;
                }
            };
        }
    ]);

})();