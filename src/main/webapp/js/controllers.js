(function () {
    'use strict';

    angular.module('app').controller('SigninCtrl', ['$scope', '$auth', '$logger', '$state', '$timeout', '$utils',
        function ($scope, $auth, $logger, $state, $timeout, $utils) {

            $logger.debug("SignCtrl.load");
            $scope.loading = false;
            $scope.success = false;
            $scope.fail = false;

            $scope.username = $scope.password = "admin";

            var success = function () {
                $logger.debug("SignCtrl.success");
                $scope.loading = false;
                $scope.fail = false;
                $scope.success = true;
                $timeout(function () {
                    $logger.debug("SignCtrl go -> index");
                    $scope.success = false;
                    $state.go('index');
                }, 500);
            };

            var fail = function (response) {
                $logger.debug("SignCtrl.fail");
                $scope.loading = false;
                $scope.success = false;
                $scope.fail = $utils.getError(response).reason;
            };

            $scope.signin = function () {
                $logger.debug("SignCtrl.signin");
                $scope.loading = true;
                $auth.signin({username: $scope.username, password: $scope.password}, success, fail);
            };

        }]);

    angular.module('app').controller('HomeCtrl', ['$scope', '$localStorage', '$auth', '$log',
        function ($scope, $localStorage, $auth, $log) {
            $log.debug("HomeCtrl.load");
            $scope.token = $localStorage.token;
            $scope.tokenClaims = $auth.getTokenClaims();
        }]);

    angular.module('app').controller('TopMenuCtrl', ['$scope', '$auth', '$state', '$logger', 'info',
        function ($scope, $auth, $state, $logger, info) {
            $logger.debug("TopMenuCtrl.load");
            $scope.info = info;
            $scope.user = $auth.getUser();
            $scope.logout = function () {
                $auth.logout(function () {
                    $logger.debug("TopMenuCtrl go -> index");
                    $state.go("index");
                });
            };
            $scope.showInfo = function () {
                $("#appInfoModal").modal('show');
            };
        }]);

    angular.module('app').controller('MainCtrl', ['$scope', '$auth', '$state', '$logger', '$config', '$http', 'urls',
        function ($scope, $auth, $state, $logger, $config, $http, urls) {
            $logger.info("MainCtrl.load");
            $scope.init = function () {
                if ($auth.isAuth()) {
                    $logger.info("MainCtrl.init isAuth");
                    // se autenticato reload config
                    $config.init(function () {
                        $logger.debug("MainCtrl go -> home");
                        $state.go("home");
                    });
                } else {
                    $logger.info("MainCtrl.init !isAuth");
                    $logger.debug("MainCtrl go -> signin");
                    $state.go("signin");
                }
            };
            $scope.$on("$TokenExpired", function () {
                $logger.info("MainCtrl.$TokenExpired");
                if ($config.isInit()) {
                    $("#expiredModal").modal({keyboard: false});
                } else {
                    $logger.debug("MainCtrl.$TokenExpired go -> signin");
                    $state.go("signin");
                }
            });
            //non in $logger perchè crea dipendenza ciclica con $http > $httpInterceptor
            $scope.$on("$LogFlush", function (data) {
                $http.post(urls.logflush, data);
            });
            $scope.init();
        }]);

})();