var FilterOperator = {
  LIKE: "LIKE", NOT_LIKE: "NOT_LIKE", IN: "IN", NOT_IN: "NOT_IN", EQUALS: "EQUALS", NOT_EQUALS: "NOT_EQUALS",
  GREATER: "GREATER", GREATER_AND_EQUALS: "GREATER_AND_EQUALS",
  LESS: "LESS", LESS_AND_EQUALS: "LESS_AND_EQUALS",
  BETWEEN: "BETWEEN", NOT_BETWEEN: "NOT_BETWEEN"
};

var FilerView = {
  TEXT: 'text',
  DATE: 'date',
  COMBO: 'combo',
  NUMBER: 'number',
  EMAIL: 'email'
};

var FilerType = {
  TEXT: 'TEXT',
  DATE: 'DATE',
  NUMBER: 'NUMBER'
};

function ComboValues(header, value) {
  this.header = header;
  this.value = value;
}

function Filterable(view, type, operator, values) {
  /**
   * FilerView
   */
  this.view = view;
  /**
   * FilerType
   */
  this.type = type;
  /**
   * FilterOperator
   */
  this.operator = operator;
  /**
   * ComboValues[]
   */
  this.values = values || [];
}

function FilterableText() {
  return new Filterable(FilerView.TEXT, FilerType.TEXT, FilterOperator.LIKE);
}

function FilterableNumber() {
  return new Filterable(FilerView.NUMBER, FilerType.NUMBER, FilterOperator.EQUALS);
}

function FilterableDate() {
  return new Filterable(FilerView.DATE, FilerType.DATE, FilterOperator.BETWEEN);
}

(function () {
  'use strict';

  angular.module('app').filter('range', function () {
    return function (input, total) {
      total = parseInt(total);
      for (var i = 0; i < total; i++)
        input.push(i);
      return input;
    };
  });

  angular.module('app').directive("error", function () {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        text: '='
      },
      template: '<div class="text-danger">' +
              '   <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>' +
              '   <p ng-if="text">{{text|translate}}</p>' +
              '  </div>'
    };
  });

  angular.module('app').directive("loading", function () {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        size: '@',
        text: '@'
      },
      template: //'<div>' +
              '   <i class="fa fa-spinner fa-pulse" ng-class="{\'fa-2x\': size && size === \'large\'}" aria-hidden="true"></i>' +
              '   <p ng-if="text && text === \'true\'" translate>loading</p>'
              //'  </div>'
    };
  });

  angular.module('app').directive("grid", ['$utils', '$debounce', '$http', '$logger', function ($utils, $debounce, $http, $logger) {
      return {
        restrict: 'E',
        transclude: true,
        scope: {
          config: '='
        },
        templateUrl: './partials/grid.html',
        link: function (scope, element, attrs) {
          //$log.info(scope.config);

          scope.debug = false;

          scope.limits = [10, 20, 30, 50];
          scope.sorter = {property: '', direction: ''};
          scope.range = {limit: 20, offset: 1};
          scope.filter = {};
          scope.total = 0;
          scope.items = [];
          scope.loading = false;
          scope.error = false;
          scope.errorMsg = '';

          scope.getItems = function () {
            return scope.items;
          };

          // check column //
          var setIndeterminate = function (check) {
            $("#check-all").prop("indeterminate", check);
            //document.getElementById("check-all").indeterminate = check;  
          };
          scope.checkAll = {
            isCheck: false
          };
          scope.checkAll = function () {
            for (var i = 0; i < $utils.count(scope.items); i++) {
              scope.items[i].checked = scope.checkAll.isCheck;
            }
            if (!scope.checkAll.isCheck) {
              setIndeterminate(false);
            }
          };
          scope.changeCheck = function (event, r) {
            event.stopPropagation();
            var totCheck = scope.getNumRowChecked();
            var totItems = $utils.count(scope.getItems());
            scope.checkAll.isCheck = r.checked ? totItems === totCheck : false;
            setIndeterminate(!scope.checkAll.isCheck && totCheck > 0);
          };
          var getRowChecked = function () {
            var checked = [];
            for (var i = 0; i < $utils.count(scope.items); i++) {
              if (scope.items[i].checked) {
                checked.push(scope.items[i]);
              }
            }
            return checked;
          };
          scope.getNumRowChecked = function () {
            return $utils.count(getRowChecked());
          };

          // props and utils //

          var _is = function (p) {
            return scope.config[p] || false;
          };
          scope.displayBtnLabel = function () {
            return _is('displayBtnLabel');
          };
          scope.isHover = function () {
            return _is('hover');
          };
          scope.isStriped = function () {
            return _is('striped');
          };
          scope.isSelectable = function () {
            return _is('selectable');
          };
          scope.getColspan = function () {
            var cols = $utils.count(scope.getColumns());
            cols += 1;//for actions
            if (scope.isSelectable())
              cols += 1;
            return cols;
          };
          scope.getColumns = function () {
            return scope.config.columns || [];
          };

          scope.getToolbar = function () {
            return scope.config.toolbar || [];
          };

          // actions //

          scope.getActions = function () {
            return scope.config.actions || [];
          };
          scope.fireAction = function (act, row) {
            act.callbackFn([row]);
          };
          scope.fireActionGroup = function (act) {
            act.callbackFn(getRowChecked());
          };

          // filetr //

          scope.filterValidator = {};
          scope.filterStatus = {};

          if (scope.config.columns) {
            for (var i in scope.config.columns) {
              var index = scope.config.columns[i].dataIndex;
              scope.filterValidator[index] = true;
              scope.filterStatus[index] = false;
              scope.filter[index] = null;
            }
          }

          scope.filterBlur = function (c) {
            var value = scope.filter[c.dataIndex];
            var rs = true;
            if (value) {
              var view = c.filterable.view;
              switch (view) {
                case FilerView.NUMBER:
                  rs = $utils.isNumber(value);
                  break;
                case FilerView.TEXT:
                  rs = $utils.isPlainText(value);
                  break;
                case FilerView.EMAIL:
                  rs = $utils.isEmail(value);
                  break;
                  //for DATE see fDate
              }
            }
            scope.filterValidator[c.dataIndex] = rs;
            if (rs && scope.filterStatus[c.dataIndex]) {
              scope.filterStatus[c.dataIndex] = false;
              scope.refresh();
            }
          };

          scope.getFilterDefVal = function (c) {
            var i = 0, found = false;
            while (i < $utils.count(c.filterableValues) && !found) {
              if (c.filterableValues[i]["def"])
                found = true;
              else
                i++;
            }
            return (found) ? c.filterableValues[i].value : null;
          };

          // paginator //

          scope.next = function () {
            scope.range.offset++;
            scope.refresh();
          };
          scope.prev = function () {
            scope.range.offset--;
            scope.refresh();
          };
          scope.numberOfPages = function () {
            return Math.ceil(scope.total / scope.range.limit);
          };
          scope.changeLimit = function (limit) {
            scope.range.limit = limit;
            scope.refresh();
          };

          // refresh method //

          scope.refresh = function () {
            $debounce(_refresh);
          };

          //private
          function Field(dataIndex, value, type, operator) {
            this.dataIndex = dataIndex;
            this.value = value;
            this.type = type;
            this.operator = operator;
          }

          var _refresh = function () {
            var ts = "";
            var paged = {range: scope.range, sorter: scope.sorter, filter: {relation: 'AND', fields: []}};
            for (var i = 0; i < $utils.count(scope.getColumns()); i++) {
              var c = scope.config.columns[i];
              var index = c.dataIndex;
              var value = scope.filter[index];
              if (index !== 'ts' && scope.filterValidator[index] && value) {
                var f = c.filterable;
                paged.filter.fields.push(new Field(index, value, f.type, f.operator));
              }
              if (index === 'ts') {
                  ts = value;
              }
            }

            scope.total = 0;
            scope.loading = true;
            scope.error = false;
            scope.items = [];
            scope.checkAll.isCheck = false;
            scope.checkAll();

            var success = function (res) {
              $logger.info(res);
              scope.loading = false;
              scope.items = res.items;
              scope.total = res.total;
            };

            var error = function (res) {
              var ex = $utils.getError(res);
              scope.loading = false;
              scope.error = true;
              scope.errorMsg = ex.reason;
            };

            $http.get(scope.config.url, {params: {paged: paged, ts: ts}}).success(success).error(error);

          };

          scope.config.update = function () {
            scope.refresh();
          };

          scope.refresh();
        }
      };
    }]);

  angular.module('app').directive("fDate", function () {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        validator: '=',
        model:'=',
        onChange: '&'
      },
      template:
              '<button id="btn-{{id}}" ng-click="toogle()" ng-class="{\'invalid-input\':!(validator=!date.invalid)}" type="button" class="btn btn-sm btn-outline-secondary">' +
              '{{from()}} {{to()}}' +
              '</button>' +
              '<div class="card" id="card-{{id}}" style="right:2%;visibility:hidden;width:24rem;z-index:99999;position:absolute;">' +
              '    <div class="card-body">' +
              '      <date-picker-range value="date"></date-picker-range>' +
              '    </div>' +
              '</div>'
      ,
      link: function (scope, element, attrs) {
        scope.id = new Date().getTime();
        scope.date = {
          from: 0,
          to: 0,
          invalid: false
        };
        function format(ts) {
          var f = {dd: "--", mm: "--", yy: "----"};
          if (ts > 0) {
            var date = moment(ts);
            f.dd = date.get("date");
            f.mm = parseInt(date.get("month")) + 1;
            f.yy = date.get("year");
          }
          //TODO: add pattern i18n
          return (f.dd + "/" + f.mm + "/" + f.yy);
        }
        scope.to = function () {
          return format(scope.date.to);
        };
        scope.from = function () {
          return format(scope.date.from);
        };
        var startup = false;
        scope.up = true;
        scope.toogle = function () {
          if (!startup) {
            //TODO: FIX 
            $('#card-' + scope.id).css("visibility", "visible");
            startup = true;
          }
          if (scope.up) {
            $('#card-' + scope.id).slideDown({
              duration: 250,
              complete: function () {
                $('#btn-' + scope.id).button('toggle');
              }
            });
            scope.up = false;
          } else {
            $('#card-' + scope.id).slideUp({
              duration: 250,
              complete: function () {
                $('#btn-' + scope.id).button('toggle');
              }
            });
            scope.up = true;
            if (!scope.date.invalid) {
              var m = scope.model ? JSON.stringify(scope.model) : "";
              var d = JSON.stringify(scope.date);
              if (d !== m) {
                scope.model = angular.copy(scope.date);
                scope.onChange();
              }
            }
          }
        };
      }
    };
  });

  angular.module('app').directive("fCombo", function () {
    return {
      restrict: 'E',
      transclude: true,
      template: '<select ng-model="filter[c.dataIndex]"' +
              '    ng-init="filter[c.dataIndex] = getFilterDefVal(c)"' +
              '    ng-change="refresh()"' +
              '    name="fin-{{c.dataIndex}}"' +
              '    class="form-control form-control-sm">' +
              '      <option ng-repeat="v in c.filterable.values" ng-value="v.value">{{v.header|translate}}</option>' +
              '  </select>'
    };
  });

  angular.module('app').directive("fText", function () {
    return {
      restrict: 'E',
      transclude: true,
      template: '<input ng-model="filter[c.dataIndex]"' +
              '    ng-blur="filterBlur(c)"' +
              '    ng-change="filterStatus[c.dataIndex] = true"' +
              '    ng-focus="filterStatus[c.dataIndex] = false"' +
              '    ng-class="{\'invalid-input\':!filterValidator[c.dataIndex]}"' +
              '    name="fin-{{c.dataIndex}}"' +
              '    type="text" class="form-control form-control-sm">' +
              '  <!--small class="form-text text-muted">Valore non valido</small-->'
    };
  });

  angular.module('app').directive("sortable", function () {
    return {
      restrict: 'A',
      transclude: true,
      scope: {
        order: '=',
        sort: '=',
        callbackFn: '&'
      },
      template: '<a ng-click="sort_by(order)" style="color: #555555;" class="no-underline">' +
              '    <span ng-transclude></span>' +
              '    <i ng-class="selectedCls(order)"></i>' +
              '</a>',
      link: function (scope, element, attrs) {
        scope.sort_by = function (newProperty) {
          var sort = scope.sort;
          if (!sort.direction) {
            sort.direction = 'DESC';
          }
          if (sort.property == newProperty) {
            sort.direction = (sort.direction == 'DESC') ? 'ASC' : 'DESC';
          }
          sort.property = newProperty;
          scope.callbackFn();
        };
        scope.selectedCls = function (column) {
          if (column == scope.sort.property) {
            return ('fa fa-chevron-' + ((scope.sort.direction == 'DESC') ? 'down' : 'up'));
          } else {
            return'fa fa-sort';
          }
        };
      }
    };
  });

  angular.module('app').directive("datePicker", ['$utils', '$logger', function ($utils, $logger) {
      return {
        restrict: 'E',
        transclude: true,
        template: '<small>{{label|translate}}</small>' +
                '<div class="d-flex flex-row">' +
                '    <select ng-model="day" ng-change="rebind()" ng-class="{\'invalid-input\':isInvalid()}" style="width: 8rem;" class="form-control form-control-sm">' +
                '      <option ng-repeat="d in days" value="{{d.index}}">{{ "d_" + d.day |translate }} {{d.index}}</option>' +
                '    </select>' +
                '    &sol;' +
                '    <select ng-model="month" ng-change="change()" ng-class="{\'invalid-input\':isInvalid()}" style="width: 8rem;" class="form-control form-control-sm">' +
                '      <option ng-repeat="m in months" ng-value="m">{{ "m_" + m |translate }}</option>' +
                '    </select>' +
                '    &sol;' +
                '    <select ng-model="year" ng-change="change()" ng-class="{\'invalid-input\':isInvalid()}" style="width: 8rem;" class="form-control form-control-sm">' +
                '      <option ng-repeat="y in years" ng-value="y">{{y}}</option>' +
                '    </select>' +
                '</div>',
        scope: {
          value: '=',
          invalid: '=',
          label: '@'
        },
        link: function (scope, element, attrs) {

          scope.reset = function () {
            scope.day = scope.month = scope.year = -1;
          };

          if (scope.value) {
            var date = moment(scope.value);
            scope.day = date.get("date");
            scope.month = date.get("month");
            scope.year = date.get("year");
          } else {
//                        var now = new Date();
//                        scope.day = now.getDate();
//                        scope.month = now.getMonth();
//                        scope.year = now.getFullYear();
            scope.reset();
          }

          scope.isInvalid = function () {
            return $utils.isBool(scope.invalid);
          };

          scope.days = [];
          scope.years = [];
          scope.months = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

          for (var y = 2010; y <= (new Date().getFullYear()); y++) {
            scope.years.push(y);
          }

          scope.reconfigure = function () {
            var start = moment({year: scope.year, month: scope.month, date: scope.day});
            scope.days = [];
            for (var i = 0; i < start.daysInMonth(); i++) {
              var index = i + 1;
              var date = moment({year: scope.year, month: scope.month, date: index});
              var day = date.day();
              scope.days.push({
                index: index,
                day: day
              });
            }
          };

          scope.change = function () {
            scope.day = 1;
            scope.reconfigure();
            scope.rebind();
          };

          scope.rebind = function () {
            var date = {year: scope.year, month: scope.month, date: scope.day};
            var m = moment(date);
            scope.value = m.valueOf();
            $logger.info("rebind -> value=" + scope.value);
          };

          if (scope.value)
            scope.reconfigure();

          scope.$on("$ResetDatePicker", function () {
            $logger.debug("$ResetDatePicker");
            scope.reset();
            scope.reconfigure();
          });
        }
      };
    }]);

  angular.module('app').directive("datePickerRange", ['$utils', '$logger', '$rootScope', function ($utils, $logger, $rootScope) {
      return {
        restrict: 'E',
        transclude: true,
        scope: {
          value: '='
        },
        template: '<div class="d-flex flex-column">' +
                '   <date-picker value="value.from" label="from" invalid="value.invalid"></date-picker>' +
                '   <date-picker value="value.to" label="to" invalid="value.invalid"></date-picker>' +
                '   <button ng-click="reset()" style="width:0rem" type="button" class="btn btn-sm btn-link" >' +
                '     <!--i class="fa fa-undo"></i--> Reset' +
                '   </button>' +
                '</div>',
        link: function (scope, element, attrs) {
          //scope.range.invalid = false;

          var validate = function () {
            scope.value.invalid = scope.value.from > scope.value.to;
            $logger.info("value changed " + JSON.stringify(scope.value));
          };

          scope.$watch('value.from', validate);
          scope.$watch('value.to', validate);

          scope.reset = function () {
            scope.value.invalid = false;
            scope.value.from = scope.value.to = 0;
            $rootScope.$broadcast('$ResetDatePicker');
          };

        }

      };
    }
  ]);
  
  angular.module('app').controller('TestCtrl', ['$scope', '$logger', 'urls', '$utils',
    'Upload', '$timeout', '$http', '$rootScope',
    function ($scope, $logger, urls, $utils, Upload, $timeout, $http, $rootScope) {

      $scope.range = {
        from: 0,
        to: 0,
        invalid: false
      };

      var columns = [];

      columns.push({
        tabIndex: 1,
        header: "file-name",
        dataIndex: "name",
        flex: 1,
        sortable: true,
        filterable: new FilterableText()
      });
      columns.push({
        tabIndex: 2,
        header: "Field 1",
        dataIndex: "field1",
        flex: 1,
        sortable: true,
        filterable: new FilterableText()
      });
      columns.push({
        tabIndex: 3,
        header: "Field 2",
        dataIndex: "field2",
        flex: 1,
        sortable: true,
        filterable: new FilterableText()
      });
      columns.push({
        tabIndex: 4,
        header: "upload-date",
        dataIndex: "ts",
        flex: 1,
        sortable: true,
        filterable: new FilterableDate()
      });
      columns.push({
        tabIndex: 5,
        header: "Size",
        dataIndex: "size",
        flex: 1,
        sortable: false
      });

      var actions = [];

      function  extend(destination, source) {
        for (var property in source) {
          destination[property] = source[property];
        }
        return destination;
      }

      function transformRequest(obj) {
        var str = [];
        for (var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      }

      function download(par) {
        par = extend({_dc: new Date().getTime()}, par);
        var strPath = "Download?" + transformRequest(par);
        try {
          //console.log("smuDownload: " + strPath);
        } catch (err) {
        }
        var iframe = document.getElementById("hiddenDownloader");
        if (iframe == null) {
          iframe = document.createElement('iframe');
          iframe.id = "hiddenDownloader";
          iframe.style.visibility = 'hidden';
          document.body.appendChild(iframe);
        }
        iframe.src = strPath;
        return false;
      }

      actions.push({
        title: "Download",
        icon: 'fa fa-download',
        multiple: true,
        callbackFn: function (rows) {
          $logger.info(rows || []);
          if ($utils.count(rows) === 1) {
            download({hash: rows[0].hash});
          } else if ($utils.count(rows) > 1) {
            var hash = [];
            for (var i in rows) {
              hash.push(rows[i].hash);
            }
            download({hash: JSON.stringify(hash), multi: 1});
          }
        }
      });
      
      $scope.daDel = [];
      $scope.removeAll = function(){
        $http.get(urls.baseUrl + "Remove", {params: {hash: JSON.stringify($scope.daDel)}})
                    .success(function () {
                      //addAlert("success","Operazione effettuata con successo :)");
                      $scope.gridConfig.update();
                    }).error(function () {
                      alert("Errore durante l'esecuzione :(");
            });
      };
      actions.push({
        title: "delete",
        icon: 'fa fa-trash-alt',
        multiple: true,
        callbackFn: function (rows) {
          $logger.info(rows || []);
          if ($utils.count(rows) > 0) {
            var hash = [];
            for (var i in rows) {
              hash.push(rows[i].hash);
            }
            $scope.daDel = hash;
            $("#deleteModal").modal("show");
          }
        }
      });

      var _file = {};
      $scope.getFileName = function () {
        return _file.name;
      };
      var toolbar = [{
          title: "Upload",
          icon: 'fa fa-upload',
          callbackFn: function (rows) {
            $logger.info("upload");
            _file = {};
            $scope.data.fail = $scope.data.field1 = $scope.data.field2 = '';
            $scope.data.loading = $scope.data.upload = false;
            $scope.data.valid1 = $scope.valid2 = $scope.data.validFile = true;
            uploadAttachReset();
            $("#uploadModal").modal({keyboard: false});
          }
        }];

      $scope.gridConfig = {
        url: urls.baseUrl + "GetDocuments",
        columns: columns,
        striped: false,
        hover: true,
        selectable: true,
        displayBtnLabel: false,
        actions: actions,
        toolbar: toolbar
      };

      var uploadAttachReset = function () {
        for (var p in uploadAttachModel)
          $scope.attach[p] = uploadAttachModel[p];
      };

      var uploadAttachIsValid = function () {
        return !($scope.attach.invalidFiles && $scope.attach.invalidFiles.$error) && $scope.attach.file;
      };

      var uploadAttachModel = {
        progress: 0, // percentuale di uploading
        progressEndCls: '', // css del messaggio di fine upload
        progressError: '', // eventuale messaggio di errore di upload
        maxSize: "5MB",
        invalidFiles: {},
        file: ''
      };

      $scope.attach = angular.copy(uploadAttachModel);

      $scope.data = {
        fail: '',
        loading: false,
        upload: false,
        field1: '',
        field2: '',
        valid1: true,
        valid2: true,
        validFile: true
      };

      $scope.uploadAttach = function () {

        $logger.debug("uploadAttach()");
        $logger.info($scope.attach);

        if (!uploadAttachIsValid()) {
          $logger.warn("Attachment in error");
          return false;
        }
        $scope.data.upload = false;
        $logger.debug("upload()");
        Upload.upload({
          url: urls.baseUrl + 'Upload?_dc=' + new Date().getTime(),
          data: {file: $scope.attach.file}
        }).then(function (resp) {
          $logger.debug(resp);
          $scope.attach.progressEndCls = 'bg-success';
          _file = resp.data;
          $timeout(function () {
            $scope.data.upload = true;
            uploadAttachReset();
          }, 500);

        }, function (resp) {
          $logger.error('Error status: ' + resp.status);
          $scope.attach.progressError = 'Error status: ' + resp.status;
          $scope.attach.progressEndCls = 'bg-warning';
          _file = '';
        }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $scope.attach.progress = progressPercentage;
          $logger.debug('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
      };

      $scope.save = function () {

        $scope.data.valid1 = $utils.count($scope.data.field1) > 0;
        $scope.data.valid2 = $utils.count($scope.data.field2) > 0;
        $scope.data.validFile = $scope.data.upload;

        if ($scope.data.valid1 && $scope.data.valid2 && $scope.data.upload) {
          $scope.data.loading = true;
          _file = extend(_file, $scope.data);
          $http.post(urls.baseUrl + "Save", {
            data: JSON.stringify(_file)
          })
                  .success(function () {
                    $scope.data.loading = false;
                    $("#uploadModal").modal("hide");
                    //addAlert("success","Operazione effettuata con successo :)");
                    $scope.gridConfig.update();
                  }).error(function (res) {
            $scope.data.loading = false;
            $scope.data.fail = "Errore durante l'esecuzione :(";
          });

        }
      };

    }]);
})();


