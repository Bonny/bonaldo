package com.bonaldo.app;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "GetDocuments", urlPatterns = {"/GetDocuments"})
public class GetDocuments extends HttpServlet {

    public static void printParameters(final HttpServletRequest request) {
        final Enumeration<String> parameterNames = request.getParameterNames();
        final String lofPrefix = "[" + request.getSession().getId() + "] ";
        while (parameterNames.hasMoreElements()) {
            String key = parameterNames.nextElement();
            String value = request.getParameter(key);
            System.out.println(lofPrefix + "# [" + key + ":" + value + "]");
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            Thread.sleep(800);

            printParameters(request);

            final Store s = Store.getInstance();

//            s.clear();
//            for (int i = 0; i < 5; i++) {
//                Document d = new Document();
//                d.setHash("hash-" + i);
//                d.setName("Documento-" + (i + 1) + ".pdf");
//                d.setPayload(new byte[i * 1000]);
//                d.setField1("Example 1 " + i * 10);
//                d.setField2("Example 2 " + i * 10);
//                s.add(d);
//            }

            String paged = request.getParameter("paged");
            System.out.println("paged=" + paged);
            PagedRequest pr = parse(paged);

            PagedResponse<JsonObject> res = new PagedResponse<JsonObject>();
            res.setItems(new ArrayList<JsonObject>());

            long from = 0, to = 0;

            try {
                System.out.println("ts=" + request.getParameter("ts"));
                JsonObject o = new Gson().fromJson(request.getParameter("ts"), JsonObject.class);
                from = Long.valueOf(o.get("from").getAsString());
                to = Long.valueOf(o.get("to").getAsString());
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }

            System.out.println("from=" + from + ", to=" + to);
            Date now = new Date();
            System.out.println("from=" + new Date(from) + ", to=" + new Date(to) + " -> " + (now.after(new Date(from)) && now.before(new Date(to))));
            
   
            for (Document d : s) {

                boolean match = true;

                for (Field f : pr.getFilter().getFields()) {
                    if (f.getDataIndex().equals("name")) {
                        System.out.println("name");
                        match = match && d.getName().contains(f.getValue());
                    }
                    if (f.getDataIndex().equals("field1")) {
                        System.out.println("field1");
                        match = match && d.getField1().contains(f.getValue());
                    }
                    if (f.getDataIndex().equals("field2")) {
                        System.out.println("field2");
                        match = match && d.getField2().contains(f.getValue());
                    }
                }

                if (from > 0 && to > 0) {
                    match = match && (d.getTs().after(new Date(from)) && d.getTs().before(new Date(to)));
                }

                if (match) {
                    res.getItems().add(d.toJson());
                }
            }

            res.setTotal(Utils.count(res.getItems()));
            
            out.println(new Gson().toJson(res));

        } catch (Exception ex) {
            ex.printStackTrace();
            out.println(ErrorCodes.BAD_REQUEST.toJson());
        } finally {
            out.close();
        }
    }

    private GridRange parseRange(JsonObject json) {
        GridRange gr = new GridRange();
        try {
            JsonObject range = json.getAsJsonObject("range");
            gr.setLimit(range.get("limit").getAsInt());
            gr.setOffset(range.get("offset").getAsInt());
        } catch (Exception ex) {
            System.err.println("Parse [Range] error " + ex);
        }
        return gr;
    }

    private GridSorter parseSorter(JsonObject json) {
        GridSorter gs = new GridSorter();
        try {
            JsonObject sorter = json.getAsJsonObject("sorter");
            gs.setProperty(sorter.get("property").getAsString());
            gs.setDirection(sorter.get("direction").getAsString());
        } catch (Exception ex) {
            System.err.println("Parse [Sorter] error " + ex);
        }
        return gs;
    }

    private GridFilter parseFilter(JsonObject json) {
        GridFilter gf = new GridFilter();
        try {
            gf = new Gson().fromJson(json.getAsJsonObject("filter"), GridFilter.class);
//            JsonObject filter = json.getAsJsonObject("filter");
//            if (filter.has("relation")) {
//                gf.setRelation(filter.get("relation").getAsString());
//            }
//            JsonObject fields = filter.getAsJsonObject("fields");
//            Set<Map.Entry<String, JsonElement>> set = fields.entrySet();
//            set.forEach((e) -> {
//                //gf.updateField(e.getKey(), e.getValue().getAsString());
//            });
        } catch (Exception ex) {
            System.err.println("Parse [Filter] error " + ex);
        }
        return gf;
    }

    private PagedRequest parse(String paged) {
        final PagedRequest pr = new PagedRequest();
        try {
            Gson gson = new Gson();
            JsonObject json = gson.fromJson(paged, JsonObject.class);
            pr.setRange(parseRange(json));
            pr.setSorter(parseSorter(json));
            pr.setFilter(parseFilter(json));
        } catch (JsonSyntaxException ex) {
            System.err.println("Parse error " + ex.getMessage());
        }
        return pr;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
