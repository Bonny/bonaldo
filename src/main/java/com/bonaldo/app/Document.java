package com.bonaldo.app;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Document implements Serializable {

  String name;
  String hash;
  Date ts;
  byte[] payload;

  public String getField1() {
    return field1;
  }

  public void setField1(String field1) {
    this.field1 = field1;
  }

  public String getField2() {
    return field2;
  }

  public void setField2(String field2) {
    this.field2 = field2;
  }

  String field1;
  String field2;

  public Document() {
    ts = new Date();
    hash = "hash-" + ts.getTime();
  }

  public Document(String hash) {
    this();
    this.hash = hash;
  }

  public Document(String name, byte[] payload) {
    this();
    this.name = name;
    this.payload = payload;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  public Date getTs() {
    return ts;
  }

  public void setTs(Date ts) {
    this.ts = ts;
  }

  public byte[] getPayload() {
    return payload;
  }

  public void setPayload(byte[] payload) {
    this.payload = payload;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 29 * hash + (this.hash != null ? this.hash.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Document other = (Document) obj;
    if ((this.hash == null) ? (other.hash != null) : !this.hash.equals(other.hash)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return new Gson().toJson(toJson());
  }

  public JsonObject toJson() {
    JsonObject o = new JsonObject();
    o.addProperty("name", getName());
    o.addProperty("hash", getHash());
    o.addProperty("ts", new SimpleDateFormat("dd/MM/yyy").format(getTs()));
    o.addProperty("size", Utils.human_length(getPayload()));
    o.addProperty("field1", getField1());
    o.addProperty("field2", getField2());
    return o;
  }

}
