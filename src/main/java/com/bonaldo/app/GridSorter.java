package com.bonaldo.app;

import java.io.Serializable;

/**
 * Per ordinare una colonna
 *
 * @author lbonaldo
 *
 */
public class GridSorter implements Serializable {

    private static final long serialVersionUID = 10L;

    private String property;
    private String direction;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "GridSorter[" + "property=" + property + ", direction=" + direction + ']';
    }

}
