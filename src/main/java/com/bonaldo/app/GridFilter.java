package com.bonaldo.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Per filtrare i dati inviati dal client javascript
 *
 * @author lbonaldo
 *
 */
public class GridFilter implements Serializable {

    private static final long serialVersionUID = 12L;

    private String relation = "AND";

    private List<Field> fields;

    public static enum Type {
        DATE, NUMBER, TEXT, BOOL
    }

    public static enum Operator {

        LIKE(" like "), NOT_LIKE(" not like "), IN(" in "), NOT_IN(" not in "), EQUALS(" = "), NOT_EQUALS(" <> "), GREATER(" > "), GREATER_AND_EQUALS(" >= "), LESS(" < "), LESS_AND_EQUALS(" <= "), BETWEEN(
                " between "), NOT_BETWEEN(" not between ");

        private String opt = "";

        private Operator(String opt) {
            this.opt = opt;
        }

        public String get() {
            return this.opt;
        }
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public boolean isEmpty() {
        return getFields().isEmpty();
    }

    public List<Field> getFields() {
        if (fields == null) {
            fields = new ArrayList<Field>();
        }
        return fields;
    }

    @Override
    public String toString() {
        return "GridFilter [relation=" + relation + ", fields=" + Utils.count(getFields()) + "]";
    }

}
