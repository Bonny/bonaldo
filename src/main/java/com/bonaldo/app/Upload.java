/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonaldo.app;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author luca
 */
@WebServlet(name = "Upload", urlPatterns = {"/Upload"})
public class Upload extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            Document d = getDocument(request, "file");
            
            JsonObject o = new JsonObject();
            o.addProperty("name", d.getName());
            o.addProperty("payload", new String(Utils.encodeBase64(d.getPayload())));
            
            out.println(new Gson().toJson(o));
            
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setStatus(ErrorCodes.UNKNOW.getStatus());
            out.println(ex.getMessage());
        }
        out.close();
    }
    
    protected Document getDocument(HttpServletRequest request, String paramName) throws Exception {
        
        Document res = null;
        ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
        List<FileItem> listaFile = null;
        final Long maxSize = 0L;
        
        try {
            
            listaFile = (List<FileItem>) upload.parseRequest(request);
            
            if (listaFile == null || listaFile.isEmpty()) {
                throw new Exception("File upload is empty");
            }
            
            FileItem item = listaFile.get(0);
            
            if (item.getFieldName().startsWith(paramName)) {
                
                if (item.getSize() == 0) {
                    throw new Exception("File upload is empty");
                }
                
                if (maxSize > 0 && item.getSize() > maxSize) {
                    throw new Exception("file upload exceeds allowed size");
                }
                
                String fileName = item.getName();
                
                if (fileName.contains(System.getProperty("file.separator"))) {
                    
                    int lastIndexOf = fileName.lastIndexOf(System.getProperty("file.separator"));
                    String tmp = fileName.substring(lastIndexOf + 1);
                    res = new Document(tmp, item.get());
                    
                } else {
                    res = new Document(item.getName(), item.get());
                }
            }
        } catch (Exception e) {
            throw e;
        }
        
        return res;
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
