/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonaldo.app;

import com.google.gson.Gson;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author luca
 */
@WebServlet(name = "Download", urlPatterns = {"/Download"})
public class Download extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        byte[] bytes = null;
        final Store s = Store.getInstance();
        String filename = "";
        String cnt = "";
        String multi = request.getParameter("multi");

        try {

            String in = request.getParameter("hash");
            final HashSet<String> h = new HashSet<String>();
            final List<Document> docs = new ArrayList<Document>();

            System.out.println("in=" + in);
            System.out.println("multi=" + multi);

            if (!Utils.isNullOrEmpty(multi) && multi.equals("1")) {
                String[] list = new Gson().fromJson(in, String[].class);
                h.addAll(Arrays.asList(list));
            } else {
                h.add(in);
            }

            System.out.println("h=" + h);

            for (String i : h) {
                int of = s.indexOf(new Document(i));
                if (of >= 0) {
                    docs.add(s.get(of));
                }
            }
            
            System.out.println("docs=" + docs.size());
            if (docs.size() == 1) {
                bytes = docs.get(0).getPayload();
                filename = docs.get(0).getName();
                cnt = "application/pdf";
            } else {
                Map<String, byte[]> files = new HashMap<String, byte[]>();
                for (Document d : docs) {
                    files.put(d.getName(), d.getPayload());
                }
                bytes = zip(files);
                filename = "fascicolo-" + new Date().getTime() + ".zip";
                cnt = "application/zip";
            }

        } catch (Exception ex) {
            System.err.println(ex);
        }

        response.setContentType(cnt);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
        response.setContentLength(bytes != null ? bytes.length : 0);
        response.getOutputStream().write(bytes);
        response.getOutputStream().flush();

    }

    byte[] zip(Map<String, byte[]> files) {

        byte[] zipByte = null;

        try {

            ZipOutputStream out = null;
            byte[] buf = new byte[1024];

            ByteArrayInputStream in;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            out = new ZipOutputStream(baos);

            for (Entry<String, byte[]> file : files.entrySet()) {

                in = new ByteArrayInputStream(file.getValue());

                out.putNextEntry(new ZipEntry(file.getKey()));

                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                out.closeEntry();
                in.close();
                // progress = i*100/file.length;
            }
            out.close();
            baos.close();

            zipByte = baos.toByteArray();

        } catch (Exception e) {
            System.err.println(e);
        }

        return zipByte;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
