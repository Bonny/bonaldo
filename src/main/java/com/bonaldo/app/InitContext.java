package com.bonaldo.app;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class InitContext implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Start bonaldo app".toUpperCase());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Stop bonaldo app".toUpperCase());
    }
}
