package com.bonaldo.app;

import java.util.ArrayList;

public final class Store extends ArrayList<Document> {

    private static final Store store = new Store();

    public static final Store getInstance() {
        return store;
    }
    
}
