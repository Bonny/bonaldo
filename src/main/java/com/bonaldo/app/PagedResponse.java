package com.bonaldo.app;

import java.io.Serializable;
import java.util.List;

public class PagedResponse<T> implements Serializable {

    private static final long serialVersionUID = 18L;
    List<T> items;
    long total;

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "PagedResponse{" + "items=" + items + ", total=" + total + '}';
    }

}
