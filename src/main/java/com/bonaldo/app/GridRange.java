package com.bonaldo.app;

import java.io.Serializable;

/**
 * Per la paginazione
 *
 * @author lbonaldo
 *
 */
public class GridRange implements Serializable {

    private static final long serialVersionUID = 21L;
    
    private Integer limit = 20;
    private Integer offset = 0;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "GridRange [limit: " + (limit != null ? limit : "null") + ", offset: "
                + (offset != null ? offset : "null") + "]";
    }

}
