package com.bonaldo.app;

import com.bonaldo.app.GridFilter.Operator;
import com.bonaldo.app.GridFilter.Type;
import java.util.Objects;


/**
 *
 * @author luca
 */
public class Field {

    private Type type = Type.TEXT;
    private Operator operator = Operator.EQUALS;
    private String value = null;
    private String dataIndex = null;

    public String getDataIndex() {
        return dataIndex;
    }

    public void setDataIndex(String dataIndex) {
        this.dataIndex = dataIndex;
    }


    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Field{" + "type=" + type + ", operator=" + operator + ", value=" + value + ", dataIndex=" + dataIndex + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.operator);
        hash = 59 * hash + Objects.hashCode(this.value);
        hash = 59 * hash + Objects.hashCode(this.dataIndex);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Field other = (Field) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        if (!Objects.equals(this.dataIndex, other.dataIndex)) {
            return false;
        }
        if (this.operator != other.operator) {
            return false;
        }
        return true;
    }

    
}
