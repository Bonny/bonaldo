package com.bonaldo.app;

import java.io.Serializable;

/**
 * Query con richiesta di paginazione
 *
 * @author lbonaldo
 *
 */
public class PagedRequest implements Serializable {

    private static final long serialVersionUID = 17L;
    private GridRange range;
    private GridSorter sorter;
    private GridFilter filter;

    public GridSorter getSorter() {
        if (sorter == null) {
            sorter = new GridSorter();
        }
        return sorter;
    }

    public void setSorter(GridSorter sorter) {
        this.sorter = sorter;
    }

    public GridFilter getFilter() {
        if (filter == null) {
            filter = new GridFilter();
        }
        return filter;
    }

    public void setFilter(GridFilter filter) {
        this.filter = filter;
    }

    public GridRange getRange() {
        if (range == null) {
            range = new GridRange();
        }
        return range;
    }

    public void setRange(GridRange range) {
        this.range = range;
    }

    @Override
    public String toString() {
        return "PagedRequest[" + getRange() + ", " + getSorter() + ", " + getFilter() + "]";
    }

}
