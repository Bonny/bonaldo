package com.bonaldo.app;

/**
 *
 * @author luca
 */
public enum ErrorCodes {

    UNKNOW(500, "unknow-error"),
    BAD_REQUEST(400, "input-not-invalid"),
    NOT_FOUND(404, "not-found")

    ;
  
    private String reason;
    private int status;

    private ErrorCodes(int status, String reason) {
        this.status = status;
        this.reason = reason;
    }

    private ErrorCodes(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public int getStatus() {
        return status;
    }

    public String toJson() {
        return new StringBuilder("{ ")
                .append(String.format("\"code\" : \"%s\"", name()))
                .append(", ")
                .append(String.format("\"reason\" : \"%s\"", getReason()))
                .append(", ")
                .append(String.format("\"status\" : %d", getStatus()))
                .append(" }")
                .toString();
    }

}
