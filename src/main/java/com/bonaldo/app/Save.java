package com.bonaldo.app;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Bonny
 */
@WebServlet(name = "Save", urlPatterns = {"/Save"})
public class Save extends HttpServlet {

  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("application/json;charset=UTF-8");
    PrintWriter out = response.getWriter();
    try {
      String in = IOUtils.toString(request.getInputStream(), "utf-8");



      JsonObject all = new Gson().fromJson(in, JsonObject.class);
//      System.out.println(all);
      
      String data = all.get("data").getAsString();
      
      JsonObject o = new Gson().fromJson(data, JsonObject.class);

      Document d = new Document();
      d.setName(o.get("name").getAsString());
      d.setPayload(Utils.decodeBase64(o.get("payload").getAsString().getBytes()));
      d.setField1(o.get("field1").getAsString());
      d.setField2(o.get("field2").getAsString());
      Store.getInstance().add(d);
    } catch (Exception ex) {
      ex.printStackTrace();
      response.setStatus(ErrorCodes.UNKNOW.getStatus());
      out.println(ex.getMessage());
    }
    out.close();
  }

  public class All {
    Data data;

    public Data getData() {
      return data;
    }

    public void setData(Data data) {
      this.data = data;
    }

    @Override
    public String toString() {
      return "All{" + "data=" + data + '}';
    }
    
  }
  
  public class Data {

    String field1;
    String field2;
    String name;
    String payload;

    @Override
    public String toString() {
      return "Data{" + "field1=" + field1 + ", field2=" + field2 + ", name=" + name + ", payload=" + payload + '}';
    }

    public String getField1() {
      return field1;
    }

    public void setField1(String field1) {
      this.field1 = field1;
    }

    public String getField2() {
      return field2;
    }

    public void setField2(String field2) {
      this.field2 = field2;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getPayload() {
      return payload;
    }

    public void setPayload(String payload) {
      this.payload = payload;
    }

  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
