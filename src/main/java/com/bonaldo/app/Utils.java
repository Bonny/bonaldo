package com.bonaldo.app;

import java.util.Collection;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author luca
 */
public class Utils {

    public static final String EMAIL = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

    public static byte[] encodeBase64(byte[] data) {
        if (!isNullOrEmpty(data)) {
            return Base64.encodeBase64(data);
        } else {
            return null;
        }
    }

    public static byte[] decodeBase64(byte[] data) {
        if (!isNullOrEmpty(data)) {
            return Base64.decodeBase64(data);
        } else {
            return null;
        }
    }

    public final static String human_length(byte[] b) {
        return human_length(b != null ? b.length : 0);
    }

    public final static String human_length(int len) {
        String ordGrandezza;
        int fileSize = len;
        // Se è minore di 1 KB, possiamo scrivere in bytes
        if (fileSize < 1000) {
            ordGrandezza = "bytes";
        } else if (fileSize < 1000000) {
            // KB, dividiamo per 1000
            fileSize = fileSize / 1000;
            ordGrandezza = "KB";
        } else {
            // MegaByte, dividiamo per un milione
            fileSize = fileSize / 1000000;
            ordGrandezza = "MB";
        }
        return String.format("%d %s", fileSize, ordGrandezza);
    }

    public static String toString(Map m) {
//        StringBuffer sb = new StringBuffer();
//        if (!isNullOrEmpty(m))
//        return String.format("{%s}", sb);
        return "[]";
    }

    public static boolean isNotNullOrEmpty(String s) {
        return !isNullOrEmpty(s);
    }

    public static int count(Map m) {
        return isNullOrEmpty(m) ? 0 : m.size();
    }

    public static int count(Collection c) {
        return isNullOrEmpty(c) ? 0 : c.size();
    }

    private Utils() {
    }

    public static String getLogPrefix(String id) {
        return String.format("[sessUID=%s] ", id);
    }

    public static boolean isEmailAddress(String s) {
        return !isNullOrEmpty(s) && s.matches(EMAIL);
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static boolean isNullOrEmpty(Map m) {
        return m == null || m.isEmpty();
    }

    public static boolean isNullOrEmpty(byte[] b) {
        return b == null || b.length == 0;
    }

    public static boolean isNullOrEmpty(Collection s) {
        return s == null || s.isEmpty();
    }
}
